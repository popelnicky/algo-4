#include <iostream>
#include "deque.cpp"

using namespace std;

int main()
{
  Deque *deque = new Deque();
	
  deque->pushFront(78);
	deque->pushFront(57);
	deque->pushFront(81);
	deque->pushFront(13);

	cout << deque->popFront() << endl;

  deque->pushBack(49);

	cout << deque->popFront() << endl;
	cout << deque->popFront() << endl;
	
  cout << deque->isEmpty() << endl;

  cout << deque->popFront() << endl;
  cout << deque->popFront() << endl;

  cout << deque->isEmpty() << endl;

	cout << "Success!";
}