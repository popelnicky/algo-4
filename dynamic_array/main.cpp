// Main function of the C++ program.

#include <iostream>
#include "DynamicArray.cpp"

using namespace std;

int main() {
    DynamicArray *array = new DynamicArray(1, 1); // [1]
    array->pushBack(2); // [1, 2]
    std::cout << "size: " << array->getSize() << " capacity: " << array->getCapacity() << std::endl;
    array->popBack(); // [1]
    array->popBack(); // []
    std::cout << "size: " << array->getSize() << " capacity: " << array->getCapacity() << std::endl;

    for (size_t i = 1; i <= 30; ++i) {
        array->pushBack(i);
    }
    // [1, 2, ... 30]
    std::cout << "size: " << array->getSize() << " capacity: " << array->getCapacity() << std::endl;
    
    std::cout << "It works! Success!";

    return 0;
}
