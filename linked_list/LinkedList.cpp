#pragma once

#include "Item.cpp"

using namespace std;

class LinkedList
{
  public:
    // constructor
    LinkedList()
    {
      first = nullptr;
      last = nullptr;
    }
    
    // Put value to the end of the linked list
    void pushBack(int val)
    {
      if(last == nullptr)
      {
        this->init(val);

        return;
      }

      Item *item = new Item();

      item->val = val;
      item->prev = last;
      item->next = nullptr;

      last->next = item;
      last = item;
    }

    // Put value to the start of the linked list
    void pushFront(int val)
    {
      if(first == nullptr)
      {
        this->init(val);

        return;
      }

      Item *item = new Item();

      item->prev = nullptr;
      item->next = first;
      item->val = val;
      
      first->prev = item;
      first = item;
    }

    // Get value for index. If index out of range, then return -1
    int get(int idx)
    {
      if(first == nullptr)
      {
        return -1;
      }

      Item *result = first;
      
      while(idx > 1)
      {
        idx--;

        result = result->next;
        
        if(result == nullptr) {
          return -1;
        }
      }

      return result->val;
    }

    // Put value to the place by index. If index out of range, then do nothing
    void set(int idx, int val)
    {
      if(first == nullptr)
      {
        return;
      }

      Item *next = first;

      while(idx > 1)
      {
        idx--;

        next = next->next;

        if (next == nullptr)
        {
          return;
        }
      }

      Item *item = new Item();

      item->prev = nullptr;
      item->next = nullptr;
      item->val = val;

      if (next->prev == nullptr)
      {
        item->next = first;
        first->prev = item;

        first = item;

        return;
      }

      item->prev = next->prev;
      item->next = next;
      
      next->prev->next = item;
      next->prev = item;
    }

    // Remove last item from the linked list
    void popBack()
    {
      if(last == nullptr)
      {
        return;
      }

      if(last->prev == first)
      {
        first = nullptr;
        last = nullptr;

        return;
      }

      Item *item = last;
      Item *prev = last->prev;

      prev->next = nullptr;
      last = prev;

      delete item;
    }

    // Remove first item from the linked list. If list is empty, then do nothing. If list has one item only, then clear list
    void popFront()
    {
      if(first == nullptr)
      {
        return;
      }

      if(first->next == last)
      {
        first = nullptr;
        last == nullptr;
      }

      Item *item = first;
      Item *next = first->next;

      next->prev = nullptr;
      first = next;

      delete item;
    }

    // Returns last value. If linked list is empty, then returns -1
    int back()
    {
      if (this->isEmpty())
      {
        return -1;
      }

      return last->val;
    }

    // Returns first value. If linked list is empty, then returns -1
    int front()
    {
      if (this->isEmpty())
      {
        return -1;
      }

      return first->val;
    }

    // Checks list is empty or no
    bool isEmpty()
    {
      return first == nullptr;
    }

  private:
    Item *first;
    Item *last;

    // Fills the list if it is empty
    void init(int val)
    {
      first = new Item();
      last = new Item();

      first->prev = nullptr;
      first->next = last;
      first->val = val;

      last->prev = first;
      last->next = nullptr;
      last->val = val;
    }
};