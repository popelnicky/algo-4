#pragma once

#include "stack.cpp"

struct Queue
{
  public:
    void push(int val)
    {
      in.push(val);
    }

    int pop()
    {
      if(out.isEmpty())
      {
        if(in.isEmpty())
        {
          return -1;
        }
        
        while (!in.isEmpty())
        {
          out.push(in.pop());
        }
      }

      return out.pop();
    }
  
  private:
    Stack in, out;
};
