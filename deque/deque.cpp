#include <iostream>
#include <vector>
#include <stack>
#include <list>

using namespace std;

class Deque
{
    public:
        void pushBack(int val)
        {
            container.push_back(val);
        }

        void pushFront(int val)
        {
            container.push_front(val);
        }

        int popBack()
        {
            if(this->isEmpty())
            {
                return -1;
            }

            int result = this->back();

            container.pop_back();

            return result;
        }
        
        int popFront()
        {
            if(this->isEmpty())
            {
                return -1;
            }
            
            int result = this->front();

            container.pop_front();

            return result;
        }
        
        int front()
        {
            if(this->isEmpty())
            {
                return -1;
            }

            return container.front();
        }
        
        int back()
        {
            if(this->isEmpty())
            {
                return -1;
            }

            return container.back();
        }

        bool isEmpty()
        {
            return container.empty();
        }
    
    private:
        list<int> container;
};
