#include <iostream>
#include "LinkedList.cpp"

using namespace std;

int main()
{
  LinkedList *list = new LinkedList();

  list->pushFront(77);
  list->pushFront(20);
  list->pushFront(35);
  list->pushFront(93);

  cout << "Third item = " << list->get(3) << endl;

  list->set(3, 11);

  cout << "Third item = " << list->get(3) << endl;

  cout << "First item = " << list->front() << endl;
  list->popFront();

  cout << "First item = " << list->front() << endl;
  list->popFront();

  cout << "First item = " << list->front() << endl;
  list->popFront();

  cout << "First item = " << list->front() << endl;
  list->popFront();

  cout << "First item = " << list->front() << endl;
  list->popFront();

  cout << "Success!" << endl;

  return 0;
}