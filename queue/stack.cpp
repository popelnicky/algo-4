#pragma once

#include <vector>

using namespace std;

struct Stack
{
  public:
    void push(int val)
    {
      container.push_back(val);
    }

    int pop()
    {
      if(isEmpty())
      {
        return -1;
      }

      int result = container.back();

      container.pop_back();

      return result;
    }

    bool isEmpty()
    {
      return container.empty();
    }

  private:
    vector<int> container;
};
