#include <iostream>
#include "queue.cpp"

using namespace std;
int main()
{
  Queue queue;

  queue.push(86);
  queue.push(63);
  queue.push(28);

  cout << "First in queue = " << queue.pop() << endl;

  queue.push(51);

  cout << "Second in queue = " << queue.pop() << endl;
  cout << "Third in queue = " << queue.pop() << endl;

  queue.push(13);

  cout << "Forth in queue = " << queue.pop() << endl;
  cout << "Fifth in queue = " << queue.pop() << endl;

  cout << "Success!" << endl;

  return 0;
}