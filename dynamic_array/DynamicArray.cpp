#pragma once

#include <iostream>
#include <cassert>
#include <cstring>

/**
 * Dynamic array data structure
 */
class DynamicArray
{
    public:
        DynamicArray(size_t size = 0, int defaultValue = 0)
        {
            if(size == 0) {
                return;
            }

            this->size = 0;
            this->capacity = size;
            this->arr = new int[this->capacity];
            this->size++;
            this->arr[this->size - 1] = defaultValue;
        }

        // Push new element in the end of array
        void pushBack(int value)
        {
            if(this->size == this->capacity)
            {
                increase();
            }

            this->size++;
            this->arr[this->size - 1] = value;
        }

        // Remove last element from array
        void popBack()
        {
            this->size--;
            this->arr[this->size] = -1;
        }

        // Get number of items in the array
        int getSize() const
        {
            return this->size;
        }

        // Get current capacity
        int getCapacity() const
        {
            return this->capacity;
        }

        // Reserve space for at least newCapacity elements
        void reserve(size_t newCapacity)
        {
            if(newCapacity <= this->size)
            {
                return;
            }

            this->capacity = newCapacity;
            increase(this->capacity);
        }

        int get(size_t pos)
        {
            if(pos < this->size - 1)
            {
                return this->arr[pos];
            }

            return -1;
        }
        
        void set(size_t pos, int val)
        {
            while(this->capacity < pos)
            {
                increase();
            }

            int *temp = this->arr;

            this->arr = new int[this->capacity];

            for(int i = 0; i <= pos; i++)
            {
                if(i == pos)
                {
                    this->arr[i] = val;
                }
                else
                {
                    this->arr[i] = temp[i];
                }
            }

            for(int i = pos + 1; i < this->size; i++)
            {
                this->arr[i] = temp[i];
            }

            this->size++;

            delete temp;
        }

    private:
        int *arr;
        int  capacity;
        int  size;
        
        void increase()
        {
            this->capacity *= 2;
            increase(this->capacity);
        }
        
        void increase(size_t capacity)
        {
            int *temp = this->arr;

            this->arr = new int[capacity];

            for(int i = 0; i < this->size; i++)
            {
                this->arr[i] = temp[i];
            }

            delete temp;
        }

};